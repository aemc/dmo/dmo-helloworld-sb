#!/usr/bin/env bash

function printBegin()
{
    echo "############################################################"
    echo "### $SCRIPT_NAME"
    echo "### begin"
    echo ""
}

function printEnd()
{
    echo ""
    echo "### finished"
    echo "### $SCRIPT_NAME"
    echo "############################################################"
}

printBegin

podman container run -d -p 8080:8080 --name=dhw aemc/dmo-helloworld-sb:2.0-latest

printEnd
