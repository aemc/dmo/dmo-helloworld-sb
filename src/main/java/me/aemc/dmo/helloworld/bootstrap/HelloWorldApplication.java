package me.aemc.dmo.helloworld.bootstrap;

import me.aemc.dmo.helloworld.versioninfo.VersionInfoBootstrap;
import me.aemc.dmo.helloworld.health.DummyHealthController;
import me.aemc.dmo.helloworld.info.InfoBootstrap;
import me.aemc.dmo.helloworld.metric.MetricBootstrap;
import me.aemc.dmo.helloworld.root.RootEndpointController;
import me.aemc.dmo.helloworld.todo.TodoBootstrap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan( useDefaultFilters = false )
@Import({
  WebSecurityConfig.class,
  InfoBootstrap.class,
  TodoBootstrap.class,
  RootEndpointController.class,
  DummyHealthController.class,
  MetricBootstrap.class,
  VersionInfoBootstrap.class
})
public class HelloWorldApplication extends SpringApplication
{
  protected transient Logger log = LoggerFactory.getLogger( this.getClass() );
  
  // ----------------------------------------------------------------------------------------------
  
  public HelloWorldApplication()
  {
    //
  }
  
  public HelloWorldApplication(Class<?> sources)
  {
    super(sources);
  }
  
  // ----------------------------------------------------------------------------------------------
  
  @Override
  protected void logStartupProfileInfo( ConfigurableApplicationContext context )
  {
    super.logStartupProfileInfo( context );
    
    Environment env = context.getEnvironment();
    this.printPropertyValue( this.log, "spring.profiles.active"    , env );
    this.printPropertyValue( this.log, "spring.config.location"    , env );
    this.printPropertyValue( this.log, "spring.datasource.url"     , env );
    this.printPropertyValue( this.log, "spring.datasource.username", env );
  }
  
  private void printPropertyValue( Logger log, String propertyName, Environment env ) {
    String propValue = env.getProperty( propertyName, "<empty>" );
    log.info( propertyName + ": {}", propValue );
  }
  
  // ----------------------------------------------------------------------------------------------
  
  public static void main( String[] args )
  {
    HelloWorldApplication app = new HelloWorldApplication( HelloWorldApplication.class );
    app.run( args );
  }
}
