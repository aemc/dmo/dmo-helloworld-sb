package me.aemc.dmo.helloworld.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.inject.Inject;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{
	@Override protected void configure( HttpSecurity http ) throws Exception
	{
		http
			.authorizeRequests()
				.antMatchers( "/" ).permitAll()
				.antMatchers( "/health" ).permitAll()
				.antMatchers( "/info" ).permitAll()
				.antMatchers( "/versionInfo" ).permitAll()
				.antMatchers( "/todos" ).hasRole( "USER" )
				.antMatchers( "/actuator/**" ).hasRole( "ADMIN" )
				.anyRequest().authenticated()
			.and()
			.csrf().disable()
			.httpBasic()
			.and()
			.logout()
				.logoutRequestMatcher( new AntPathRequestMatcher( "/logout" ) )
		;
	}
	
	@Inject public void configureGlobal( AuthenticationManagerBuilder amb ) throws Exception
	{
		this.log.debug( "configureGlobal" );
		amb.inMemoryAuthentication()
			.withUser("admin" ).password( "{noop}9876" ).roles( "ADMIN", "USER" )
			.and()
			.withUser("user1" ).password( "{noop}1234" ).roles( "USER" )
		;
	}
	
	Logger log = LoggerFactory.getLogger( this.getClass() );
}
