package me.aemc.dmo.helloworld.metric;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CustomCounterMetric
{
  final               Counter counter;
  protected transient Logger  log = LoggerFactory.getLogger( this.getClass() );
  
  @Scheduled( fixedDelay = 1000 )
  void refresh(){
    log.debug( "counter: " + this.counter.count() );
    this.counter.increment();
  }
  
  public CustomCounterMetric( MeterRegistry registry ){
    this.counter = Counter
      .builder( "dmoCounter" )
      .description( "demo counter incremented every second by one" )
      .tags( "key1", "val1", "key2", "val2" )
      .register( registry );
  }
}
