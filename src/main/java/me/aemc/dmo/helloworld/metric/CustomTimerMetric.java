package me.aemc.dmo.helloworld.metric;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class CustomTimerMetric
{
  Timer timer;
  MeterRegistry registry;
  protected transient Logger log = LoggerFactory.getLogger( this.getClass() );
  
//  @Scheduled( fixedDelay = 1000 )
//  void refresh() throws InterruptedException
//  {
//    // Math.random() generates random number from 0.0 to 0.999
//    double doubleRandomNumber = Math.random() * 1000;
//    // cast the double to whole number
//    int randomNumber = (int)doubleRandomNumber;
//
//    System.out.println( "dmoTimer-startime: " + randomNumber );
//    long start = System.currentTimeMillis();
//
//    Timer.Sample sample = Timer.start();
//    Thread.sleep( randomNumber );
//    sample.stop( this.registry.timer( "dmoTimer", "area", "51" ) );
//
//    long end = System.currentTimeMillis();
//    System.out.println( "dmoTimer-duration: " + (end - start) );
//  }
  
  @Scheduled( fixedDelay = 1000 )
  void refresh() throws InterruptedException
  {
    // Math.random() generates random number from 0.0 to 0.999
    double doubleRandomNumber = Math.random() * 1000;
    // cast the double to whole number
    int randomNumber = (int)doubleRandomNumber;
    
    log.debug( "dmoTimer: " + randomNumber );
    this.timer.record( randomNumber, TimeUnit.MILLISECONDS );
  }
  
  public CustomTimerMetric( MeterRegistry registry ){
    this.registry = registry;
  
    this.timer = Timer.builder("dmoTimer")
      .publishPercentiles(0.5, 0.95) // median and 95th percentile (1)
      .publishPercentileHistogram() // (2)
//      .serviceLevelObjectives( Duration.ofMillis(10)) // (3)
      .minimumExpectedValue(Duration.ofMillis(1)) // (4)
      .maximumExpectedValue(Duration.ofSeconds(10))
      .register( registry );
  }
}
