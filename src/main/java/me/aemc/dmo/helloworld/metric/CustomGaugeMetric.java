package me.aemc.dmo.helloworld.metric;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class CustomGaugeMetric
{
  final               AtomicInteger gauge;
  protected transient Logger        log = LoggerFactory.getLogger( this.getClass() );
  
  @Scheduled( fixedDelay = 1000 )
  void refresh(){
    int value = this.gauge.getAndIncrement();
    log.debug( "dmoGauge: " + value );
  }
  
  public CustomGaugeMetric( MeterRegistry registry ){
    this.gauge = registry.gauge( "dmoGauge", new AtomicInteger(0) );
  }
}
