package me.aemc.dmo.helloworld.metric;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricBootstrap
{
	@Bean public CustomCounterMetric customMetric(MeterRegistry meterRegistry)
	{
		return new CustomCounterMetric( meterRegistry );
	}

	@Bean public CustomGaugeMetric customGaugeMetric(MeterRegistry meterRegistry){
		return new CustomGaugeMetric( meterRegistry );
	}
	
	@Bean public CustomTimerMetric customTimerMetric(MeterRegistry meterRegistry){
		return new CustomTimerMetric( meterRegistry );
	}
}
