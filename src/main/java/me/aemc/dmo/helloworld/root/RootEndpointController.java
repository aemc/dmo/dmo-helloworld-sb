package me.aemc.dmo.helloworld.root;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RootEndpointController
{
	@RequestMapping( path = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public String handleJsonRequest()
	{
		return "{ \"text\": \"Hello World\" }";
	}
}
