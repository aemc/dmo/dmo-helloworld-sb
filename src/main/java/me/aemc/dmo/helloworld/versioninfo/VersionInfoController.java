package me.aemc.dmo.helloworld.versioninfo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

class VersionInfoData
{
  public String version  = "unknown";
}

@RestController
public class VersionInfoController {
  VersionInfoConfig versionInfoConfig;
  
  public VersionInfoController( VersionInfoConfig versionInfoConfig ) {
    this.versionInfoConfig = versionInfoConfig;
  }
  
  @GetMapping( path = "/versionInfo", produces = MediaType.APPLICATION_JSON_VALUE )
  public VersionInfoData get(){
    VersionInfoData data = new VersionInfoData();
    data.version = this.versionInfoConfig.getVersion();
    return data;
  }
}
