package me.aemc.dmo.helloworld.versioninfo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration()
@ConfigurationProperties( prefix = "version-info" )
public class VersionInfoConfig
{
  String version;
  
  public String getVersion(){
    return this.version;
  }
  
  public void setVersion( String version ){
    this.version = version;
  }
}
