package me.aemc.dmo.helloworld.versioninfo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VersionInfoBootstrap
{
	@Bean public VersionInfoConfig versionInfoConfig() {
		return new VersionInfoConfig();
	}
	
	@Bean public VersionInfoController versionInfoController()
	{
		return new VersionInfoController( this.versionInfoConfig() );
	}
}
