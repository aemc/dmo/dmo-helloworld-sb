package me.aemc.dmo.helloworld.todo;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	public BaseEntity()
	{
		// intentionally empty
	}
	
	public BaseEntity( Long id )
	{
		this.id = id;
	}

	public Long getId(){ return this.id; }

	@Override
	public boolean equals( Object obj )
	{
		if( this == obj )
		{
			return true;
		}

		if( obj == null )
		{
			return false;
		}

		if( getClass() != obj.getClass() )
		{
			return false;
		}

		BaseEntity other = ( BaseEntity ) obj;
		if( this.id == null )
		{
			if( other.id != null ) return false;
		}
		else if( !this.id.equals( other.id ) )
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.id == null ) ? 0 : this.id.hashCode() );
		return result;
	}
	
	private static final long serialVersionUID = 1L;
}
