package me.aemc.dmo.helloworld.todo;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.stereotype.Service;

@Service
public class DatabaseLoader
{
	@Inject
	private TodoRepository todoRepository;
	
	@PostConstruct
	private void initDatabase()
	{
		if( this.todoRepository.count() == 0 )
		{
			Todo todo = new Todo( "Prepare Kubernetes Training", "Prepare lab environment" );
			this.todoRepository.save( todo );
			
			todo = new Todo( "Prepare CfP DevDay 2019", "???" );
			this.todoRepository.save( todo );
		}
	}
}
