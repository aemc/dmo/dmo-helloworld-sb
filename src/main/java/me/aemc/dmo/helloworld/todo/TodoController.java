package me.aemc.dmo.helloworld.todo;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class TodoController {
	@Inject
	TodoRepository todoRepository;
	
	@GetMapping( value = "/todos", produces = MediaType.APPLICATION_JSON_VALUE )
	public Iterable<Todo> get()	{
		return this.todoRepository.findAll();
	}
	
	@PostMapping( "/todos" )
	@ResponseStatus( HttpStatus.CREATED )
	public void post( @RequestBody Todo todo ) {
		this.todoRepository.save( todo );
	}
}
