package me.aemc.dmo.helloworld.todo;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SuppressWarnings( "SpringFacetCodeInspection" ) @Configuration
@EnableJpaRepositories( basePackageClasses = TodoBootstrap.class )
@EnableTransactionManagement
@EntityScan( basePackageClasses = Todo.class )
public class TodoBootstrap
{
	@Bean public DatabaseLoader databaseLoader()
	{
		return new DatabaseLoader();
	}
	
	@Bean public TodoController todoController()
	{
		return new TodoController();
	}
}
