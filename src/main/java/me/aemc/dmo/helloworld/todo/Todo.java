package me.aemc.dmo.helloworld.todo;

import javax.persistence.Entity;

@Entity
public class Todo extends BaseEntity
{
	private String title;
	private String content;
	
	protected Todo(){}
	
	public Todo( String title, String content )
	{
		this.title   = title;
		this.content = content;
	}
	
	@Override
	public String toString()
	{
		return String.format( 
			"Todo[ id=%d, title=%s, content=%s ]", this.id, this.title, this.content
		);
	}

	public String getTitle()
	{
		return this.title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public String getContent()
	{
		return this.content;
	}

	public void setContent( String content )
	{
		this.content = content;
	}
	
	private static final long serialVersionUID = 1L;
}
