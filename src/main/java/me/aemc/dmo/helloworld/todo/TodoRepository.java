package me.aemc.dmo.helloworld.todo;

import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo,Long>
{
	//
}
