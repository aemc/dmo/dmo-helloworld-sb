package me.aemc.dmo.helloworld.info;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfoBootstrap
{
	@Bean public InfoController infoController()
	{
		return new InfoController();
	}
}
