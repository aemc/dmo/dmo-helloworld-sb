package me.aemc.dmo.helloworld.info;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

class InfoData {
  protected String hostname  = "unknown";
  protected String ipAddress = "unknown";
  public String getHostname(){ return this.hostname; }
  public String getIpAddress(){ return this.ipAddress; }
}

@RestController
public class InfoController
{
  @GetMapping( path = "/info", produces = MediaType.APPLICATION_JSON_VALUE )
  public InfoData get(){
    InfoData data = new InfoData();
  
    try {
      InetAddress ip = InetAddress.getLocalHost();
      data.hostname  = ip.getHostName();
      data.ipAddress = ip.getHostAddress();
    } catch( UnknownHostException e ) {
      e.printStackTrace();
    }
    
    return data;
  }
}
