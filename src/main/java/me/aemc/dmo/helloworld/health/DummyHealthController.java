package me.aemc.dmo.helloworld.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
public class DummyHealthController
{
	Logger  log          = LoggerFactory.getLogger( this.getClass() );
	boolean upAndRunning = true;

	@RequestMapping( path = "/health", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE )
	@ResponseBody
	public String handleGetRequest( HttpServletResponse rsp )
	{
		String retVal = "UP";

		if( !this.upAndRunning ) {
			retVal = "DOWN";
			rsp.setStatus( HttpStatus.SERVICE_UNAVAILABLE.value() );
		}

		return retVal;
	}

	@RequestMapping(
		path = "/health",
		method = {RequestMethod.POST, RequestMethod.PUT},
		consumes = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	public void handlePutRequest(@RequestBody DummyHealthStatus body, HttpServletResponse rsp )
	{
		this.log.info( "b: handlePutRequest( {} )", body );

		this.upAndRunning = body.status;

		this.log.info( "e: handlePutRequest() - isUpAndRunning: {}", this.upAndRunning );
	}
}
