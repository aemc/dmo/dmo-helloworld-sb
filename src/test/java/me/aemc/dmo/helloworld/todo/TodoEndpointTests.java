package me.aemc.dmo.helloworld.todo;

import io.restassured.http.ContentType;
import me.aemc.dmo.helloworld.bootstrap.HelloWorldApplication;
import me.aemc.dmo.helloworld.support.TestBaseClass;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = HelloWorldApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TodoEndpointTests extends TestBaseClass
{
  @SuppressWarnings( "rawtypes" )
  @Test void if_todoGet_then_ok()
  {
    Iterable response =
      given()
        .header("Content-Type", "application/json")
        .auth().basic( "user1", "1234" )
      .when()
        .get("/todos")
      .then()
        .statusCode(200)
        .extract().body().as(Iterable.class);
    
    assertThat( response ).isNotNull().hasSize( 2 );
  }
  
  @Test void if_todoSaveOk_then_oneMoreEntry()
  {
    String body = "{ \"title\": \"Title\", \"content\": \"content\"}";
  
    Iterable response =
      given()
        .header("Content-Type", "application/json")
        .auth().basic( "user1", "1234" )
        .when()
        .get("/todos")
        .then()
        .statusCode(200)
        .extract().body().as(Iterable.class);
  
    int oldSize = IterableUtil.sizeOf( response );
    assertThat( oldSize ).isEqualTo( 2 );
    
    given()
      .contentType( ContentType.JSON )
      .auth().basic( "user1", "1234" )
      .body( body )
    .when()
      .post("/todos")
    .then()
      .statusCode(201);
  
    response =
      given()
        .header("Content-Type", "application/json")
        .auth().basic( "user1", "1234" )
        .when()
        .get("/todos")
        .then()
        .statusCode(200)
        .extract().body().as(Iterable.class);
  
    int newSize = IterableUtil.sizeOf( response );
    assertThat( newSize ).isEqualTo( oldSize + 1 );
  }
}
