package me.aemc.dmo.helloworld.support;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.web.server.LocalServerPort;

public class TestBaseClass
{
  @LocalServerPort int port;
  
  @BeforeEach void configureResAssured(){
    RestAssured.baseURI = "http://localhost";
    RestAssured.port = this.port;
  }
}
