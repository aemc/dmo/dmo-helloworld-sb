package me.aemc.dmo.helloworld.versioninfo;

import me.aemc.dmo.helloworld.bootstrap.HelloWorldApplication;
import me.aemc.dmo.helloworld.support.TestBaseClass;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = HelloWorldApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VersionInfoEndpointDefaultTests extends TestBaseClass
{
  
  @Test void if_infoEndpoint_then_ok()
  {
    VersionInfoData rsp =
      given()
        .header("Content-Type", "application/json")
      .when()
        .get("/versionInfo")
      .then()
        .statusCode( 200 )
        .extract().body().as( VersionInfoData.class );
    
    assertThat(rsp).isNotNull();
    assertThat(rsp.version).isNotEmpty().isEqualTo( "1.0" );
  }
}
