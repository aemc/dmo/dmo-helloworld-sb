package me.aemc.dmo.helloworld.metric;

import me.aemc.dmo.helloworld.bootstrap.HelloWorldApplication;
import me.aemc.dmo.helloworld.support.TestBaseClass;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = HelloWorldApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ActuatorTests extends TestBaseClass
{
  @Test void if_actuatorEndpoint_then_ok()
  {
    String response =
      given()
        .header("Content-Type", "application/json")
        .auth().basic( "admin", "9876" )
        .when()
        .get("/actuator")
        .then()
        .statusCode(200)
        .extract().body().asString();
    
    assertThat( response ).isNotNull();
  }
}
