package me.aemc.dmo.helloworld.health;

import me.aemc.dmo.helloworld.bootstrap.HelloWorldApplication;
import me.aemc.dmo.helloworld.support.TestBaseClass;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = HelloWorldApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HealthEndpointTests extends TestBaseClass
{
  @Test void if_healthEndpointUp_then_ok()
  {
    String response =
      given()
        .header("Content-Type", "application/json")
      .when()
        .get("/health")
      .then()
        .statusCode(200)
        .extract().body().asString();
    
    assertThat( response ).isNotNull().isEqualTo( "UP" );
  }
  
  @Test void if_healthEndpointDown_then_ok()
  {
    given()
      .header("Content-Type", "application/json")
      .body( "{\"status\": \"false\"}" )
    .when()
      .put("/health")
    .then()
        .statusCode(200);
  
    String response =
      given()
        .header("Content-Type", "application/json")
      .when()
        .get("/health")
      .then()
        .statusCode(503)
        .extract().body().asString();
    
//    assertThat( response ).isNotNull().isEqualTo( "DOWN" );
  }
}
