package me.aemc.dmo.helloworld.root;

import me.aemc.dmo.helloworld.bootstrap.HelloWorldApplication;
import me.aemc.dmo.helloworld.support.TestBaseClass;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.*;

@SpringBootTest(classes = HelloWorldApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StartEndpointTests extends TestBaseClass
{
  @Test void if_startPage_then_ok()
  {
    String response =
      given()
        .header("Content-Type", "application/json")
      .when()
        .get("/")
      .then()
        .statusCode(200)
        .extract().body().asString();
    
    assertThat(response).isNotNull().isEqualTo("{ \"text\": \"Hello World\" }");
  }
}
