package me.aemc.dmo.helloworld.info;

import me.aemc.dmo.helloworld.bootstrap.HelloWorldApplication;
import me.aemc.dmo.helloworld.support.TestBaseClass;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.*;

@SpringBootTest(classes = HelloWorldApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class InfoEndpointTests extends TestBaseClass
{
  
  @Test void if_infoEndpoint_then_ok()
  {
    InfoData rsp =
      given()
        .header("Content-Type", "application/json")
      .when()
        .get("/info")
      .then()
        .statusCode( 200 )
        .extract().body().as( InfoData.class );
    
    assertThat(rsp).isNotNull();
    assertThat(rsp.getHostname()).isNotEmpty();
    assertThat(rsp.getIpAddress()).isNotEmpty().containsAnyOf( "127.0.1.1", "127.0.0.1" );
  }
}
