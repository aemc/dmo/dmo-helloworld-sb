#!/usr/bin/env bash

function printBegin()
{
    echo "############################################################"
    echo "### $SCRIPT_NAME"
    echo "### begin"
    echo ""
}

function printEnd()
{
    echo ""
    echo "### finished"
    echo "### $SCRIPT_NAME"
    echo "############################################################"
}

printBegin

declare version=2.0
declare dockerFile=src/docker/Dockerfile2
declare now
now=$(date -u +"%Y%m%dT%H%M%SZ")

declare registry=docker.io
declare account=aemc
declare repo=dmo-helloworld-sb
declare imageName="${registry}/${account}/${repo}:${version}-"

docker buildx build --progress=plain -t "${imageName}${now}" -t "${imageName}"latest -f "${dockerFile}" .

printEnd
